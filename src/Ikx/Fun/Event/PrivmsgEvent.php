<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Fun\Event;

use Ikx\Core\Event\AbstractEvent;
use Ikx\Core\Event\EventInterface;
use Ikx\Core\Utils\MessagingTrait;

/**
 * PRIVMSG event
 * Handles all commands sent to the bot using PRIVMSG commands
 * @package Ikx\Core\Event
 */
class PrivmsgEvent extends AbstractEvent implements EventInterface {
    use MessagingTrait;

    /**
     * Event executor
     */
    public function execute()
    {
        $firstWord = $this->parts[3];
        if (mb_substr($firstWord, 0, 1) == ':') {
            $firstWord = mb_substr($firstWord, 1);
        }

        if ($firstWord == 'F') {
            $this->msg($this->parts[2], 'Respect.');
        }
    }
}