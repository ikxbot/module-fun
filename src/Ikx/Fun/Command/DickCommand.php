<?php
namespace Ikx\Fun\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Entity\Network;
use Ikx\Core\Entity\User;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

class DickCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public $threaded = true;
    public static $dickSizes = [];

    public function describe()
    {
        return __("Fetch someone's dick size");
    }

    public function run() {
        $nickname = $this->params[0] ?? $this->nickname;

        /** @var User $user */
        if ($user = $this->network->getUser($nickname)) {
            if ($user->ison($this->channel)) {
                $nickname = $user->getNickname();
            } else {
                $nickname = $this->nickname;
            }
        } else {
            $nickname = $this->nickname;
        }

        // A user's dick size may only change once every hour
        if (isset(self::$dickSizes[$nickname]) && (time() - 3600) < self::$dickSizes[$nickname]['ts']) {
            $size = self::$dickSizes[$nickname]['size'];
        } else {
            $size = rand(1,35);
            self::$dickSizes[$nickname] = [
                'ts'        => time(),
                'size'      => $size
            ];

            $this->msg($this->channel, Format::color(__("I'm measuring %s's dick size...",
                Format::color($nickname, 4)), 10));

            sleep(rand(1,8));
        }

        $ascii = "8" . str_repeat("=", $size) . "D";

        $this->msg($this->channel, Format::color($nickname, 4) . " " . Format::color(__("has a dick of"), 10) . " " .
            Format::color($ascii, 12) . " " . Format::color($size . "cm", 4));
    }
}