<?php
/**
 * This work is licensed under the Creative Commons Attribution 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
 * or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * See LICENSE for license details
 */
namespace Ikx\Fun\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Utils\MessagingTrait;
use Ikx\Core\Utils\RandomWords;

class RwCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function run() {
        $this->msg($this->channel, ucwords(implode(' ', RandomWords::get(rand(2, 6)))));
    }

    public function describe()
    {
        return __("Fetch random words");
    }
}