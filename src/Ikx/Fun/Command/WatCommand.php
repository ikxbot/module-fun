<?php
namespace Ikx\Fun\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Utils\MessagingTrait;
use Ikx\Core\Utils\RandomNames;
use Ikx\Core\Utils\RandomWords;

class WatCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public $threaded = false;

    public function describe()
    {
        return __("WAT?!");
    }

    public function run() {
        $types = ['male', 'female'];
        $n1 = RandomNames::get($types[array_rand($types, 1)] , 1)[0];
        $n2 = RandomNames::get($types[array_rand($types, 1)] , 1)[0];
        $w = RandomWords::get(1)[0];

        $this->msg($this->channel, sprintf('%s %ss %s', $n1, $w, $n2));
    }
}