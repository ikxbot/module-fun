<?php
namespace Ikx\Fun\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Entity\Network;
use Ikx\Core\Entity\Server;
use Ikx\Core\Utils\MessagingTrait;

class GreetCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public $threaded = true;

    public $greetings = [];

    public function __construct(Server $server, Network $network, string $command, array $params)
    {
        parent::__construct($server, $network, $command, $params);

        $this->greetings = [
            __('Heya %1$s, welcome to %2$s!'),
            __('Welcome %1$s ;)'),
            __('Hello %1$s, we have long awaited your arrival!'),
            __('Oi %1$s, welcome to %2$s! Have nice stay!')
        ];
    }

    public function describe()
    {
        return "Greet a person";
    }

    public function run() {
        if (!$this->params[0]) {
            $this->msg($this->channel, __("Please specify a nickname to greet ;)"));
        } else {
            $user = $this->network->getUser($this->params[0]);
            if (!$user || !$user->ison($this->channel)) {
                $this->msg($this->channel, __("I don't see %s in here", $this->params[0]));
            } else {
                $greeting = $this->greetings[array_rand($this->greetings, 1)];
                $greeting = sprintf($greeting, $user->getNickname(), $this->channel);
                $this->msg($this->channel, $greeting);
            }
        }
    }
}