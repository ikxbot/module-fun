<?php
namespace Ikx\Fun\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Entity\Network;
use Ikx\Core\Entity\User;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

class BoobCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public $threaded = true;
    public static $boobSizes = [];
    public $sizes = [
        'AA',
        'A',
        'BB',
        'B',
        'CC',
        'C',
        'DD',
        'D',
        'EE',
        'E',
        'FF',
        'F',
        'GG',
        'G',
        'HH',
        'H'
    ];

    public function describe()
    {
        return __("Fetch someone's boob size");
    }

    public function run() {
        $nickname = $this->params[0] ?? $this->nickname;

        /** @var User $user */
        if ($user = $this->network->getUser($nickname)) {
            if ($user->ison($this->channel)) {
                $nickname = $user->getNickname();
            } else {
                $nickname = $this->nickname;
            }
        } else {
            $nickname = $this->nickname;
        }

        // A user's dick size may only change once every hour
        if (isset(self::$boobSizes[$nickname]) && (time() - 3600) > self::$boobSizes[$nickname]['ts']) {
            $size = self::$boobSizes[$nickname]['size'];
        } else {
            $size = $this->sizes[array_rand($this->sizes, 1)];
            self::$boobSizes[$nickname] = [
                'ts'        => time(),
                'size'      => $size
            ];

            $this->msg($this->channel, Format::color(__("I'm measuring %s's boob size...",
                Format::color($nickname, 4)), 10));

            sleep(rand(1,8));
        }

        $this->msg($this->channel, Format::color($nickname, 4) . " " . Format::color(__("has boobs of"), 10) . " " .
            Format::color("cup " . $size, 4));
    }
}