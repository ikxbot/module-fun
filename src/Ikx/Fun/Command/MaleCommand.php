<?php
namespace Ikx\Fun\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Utils\MessagingTrait;
use Ikx\Core\Utils\RandomNames;

class MaleCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public $threaded = false;

    public function describe()
    {
        return __("Display a random male name");
    }

    public function run() {
        $this->msg($this->channel, join("", RandomNames::get('male', 1)));
    }
}