<?php
namespace Ikx\Fun\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Entity\Network;
use Ikx\Core\Entity\User;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

class GayCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function describe()
    {
        return __("See how gay someone is");
    }

    public function run() {
        $nickname = $this->params[0] ?? $this->nickname;

        /** @var User $user */
        if ($user = $this->network->getUser($nickname)) {
            if ($user->ison($this->channel)) {
                $nickname = $user->getNickname();
            } else {
                $nickname = $this->nickname;
            }
        } else {
            $nickname = $this->nickname;
        }

        $hot = rand(0,100);

        $this->msg($this->channel, Format::color($nickname, 4) . " " . Format::color("is", 10) . " " .
            Format::color($hot . '% gay', 4));
    }
}