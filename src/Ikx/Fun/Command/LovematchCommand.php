<?php
namespace Ikx\Fun\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Entity\Network;
use Ikx\Core\Entity\User;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

class LovematchCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public $threaded = false;

    public function describe()
    {
        return __("Love is in the air....");
    }

    public function run() {
        $nickOne = $this->params[0] ?? null;
        $nickTwo = $this->params[1] ?? null;

        if (!$nickOne || !$nickTwo) {
            $this->msg($this->channel, __("%s: %s <user1> <user2>", Format::bold('SYNTAX'), $this->command));
        } else {
            $match = mt_rand(0, 100);
            if ($match <= 3) {
                $text = __('It\'s time to start a fight.');
            }
            else if ($match <= 10) {
                $text = __('I wouldn\'t count on this');
            } else if($match <= 20) {
                $text = __('I don\'t think this will work out');
            } else if ($match <= 30) {
                $text = __('You\'ve seen better days');
            } else if ($match <= 40) {
                $text = __('It might work, but I won\'t bet on it');
            } else if ($match <= 50) {
                $text = __('We\'re starting to get somewhere, but it\'s not future proof');
            } else if ($match <= 60) {
                $text = __("I don't know how to tell you this, but I wouldn't buy the rings yet");
            } else if ($match <= 70) {
                $text = __("It's getting hot in herre, so take off all your clothes");
            } else if ($match <= 80) {
                $text = __("Hot love's a-coming");
            } else if ($match <= 90) {
                $text = __("A success story in the making.");
            } else if ($match <= 95) {
                $text = __("Bring out the wedding bells, because these two are in love");
            } else {
                $text = __("Cupid's arrows have hit you hard and good!");
            }

            $this->msg($this->channel, __("Love match between %s and %s: %s%% (%s)", $nickOne, $nickTwo, $match, $text));
        }
    }
}