<?php
namespace Ikx\Fun\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Entity\Network;
use Ikx\Core\Entity\User;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

class CoinCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public $threaded = false;

    public function describe()
    {
        return __("Flip one or more coins");
    }

    public function run() {
        $count = $this->params[0] ?? 1;

        if (!is_numeric($count)) {
            $count = 1;
        }

        if ($count > 9999) {
            $this->msg($this->channel, __("I can't flip that many coins."));
            return;
        }

        $this->msg($this->channel, __("Flipping %d coins...", $count));
        $heads = 0;
        $tails = 0;
        for($i = 0; $i < $count; $i++) {
            $flip = mt_rand(1,2);
            if ($flip == 1) {
                $heads++;
            } else {
                $tails++;
            }
        }

        // sleep(rand(1,3));

        if ($heads > $tails) {
            $winner = __("Heads");
        } else if($heads < $tails) {
            $winner = __("Tails");
        } else {
            $flip = mt_rand(1,2);
            if ($flip == 1) {
                $heads++;
            } else {
                $tails++;
            }

            $count++;
            $this->msg($this->channel, Format::bold(__("Result was a tie, flipping another coin...")));

            if ($heads > $tails) {
                $winner = __("Heads");
            } else {
                $winner = __("Tails");
            }
        }

        $this->msg($this->channel, __("The coin landed %s times on heads and %s times on tails", Format::bold($heads), Format::bold($tails)));
        $this->msg($this->channel, __("%s wins!", Format::bold($winner)));
    }
}